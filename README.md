![howl_icon](howl.png)

Lexer bundle for the Howl editor (http://howl.io/)

# Installation

- Clone or download into `~/.howl/bundles`.

- You may also just copy paste this:

`git clone https://gitlab.com/MarcusE1W/howl-elixir ~/.howl/bundles/`


# Contains
- **Elixir:** Lexer for the functional language Elixir (just syntax highlighting for Elixir)
