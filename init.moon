-- Copyright 2012-2018 The Howl Developers
-- License: MIT (see LICENSE.md at the top-level directory of the distribution)

mode_reg =
  name: 'elixir'
  extensions: { 'ex', 'exs' }
--  patterns: { 'wscript$', 'SConstruct$', 'SConscript$' }
--  shebangs: '[/ ]python.*$'
  create: -> bundle_load('elixir_mode')

howl.mode.register mode_reg

unload = -> howl.mode.unregister 'elixir'

return {
  info:
    author: 'Copyright 2018 The Howl Developers',
    description: 'Elixir lexer bundle',
    license: 'MIT',
  :unload
}
