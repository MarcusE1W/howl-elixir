-- Copyright 2014-2018 The Howl Developers
-- License: MIT (see LICENSE.md at the top-level directory of the distribution)

howl.util.lpeg_lexer ->
  c = capture

  ---------------------------------
  -- Identifier
  ---------------------------------

  -- this needs checking
  ident = (alpha + '_')^1 * (alpha + digit + '_')^0

  identifer = c 'identifer', ident

  ---------------------------------
  -- Keywords
  ---------------------------------

  keyword = c 'keyword', -B'.' * word {
    "is_atom", "is_binary", "is_bitstring", "is_boolean", "is_float", "is_function", "is_integer"
    "is_list", "is_map", "is_number", "is_pid", "is_port", "is_record", "is_reference", "is_tuple",
    "is_exception", "case", "when", "cond", "for", "if", "unless", "try", "receive", "send", "exit", "raise",
    "throw", "after", "rescue", "catch", "else", "do", "end", "quote", "unquote", "super", "import", "require",
    "alias", "use", "self", "with", "fn"
  }

  ---------------------------------
  -- Function definition
  ---------------------------------


    -- Elixir func/module etc. keywords
  functiondef = c 'function', -B'.' * word {
    "defstruct", "defrecordp", "defrecord", "defprotocol", "defp", "defoverridable", "defmodule",
    "defmacrop", "defmacro", "defimpl", "defexception", "defdelegate", "defcallback", "def"
    }

-- This can probably do with some expansion for all functiondef words?
-- Also, is it possible to get do and end included ?
  fdecl = c('function', any('defp', 'def')) * c('whitespace', space^1) * c('fdecl', ident)

  moduledef = c('function', 'defmodule') * c('whitespace', space^1) * c('type_def', ident)

-- TODO see how to define a sequence

-- regex = sequence {
--     c('regex', sequence {
--       '/',
--       scan_to(any('/', eol), '\\'),
--       B('/'),
--     }),
--     c('operator', S'gim'^1)^0,
--     #sequence {
--       blank^0,
--       any(S',;.)', P(-1))
--     }
--   }


  ---------------------------------
  -- Operators
  ---------------------------------


  -- wild collection of symbols, not really operators
  -- operator = c 'operator', S'+-*/%~&^=!<>;:,.(){}[]|`'
  operator = c 'operator', S'+-*/%~&^=!<>:.(){}[]|@\\'
  operator2 = c 'operator', word('and', 'or', 'not', 'when', 'xor', 'in' )

  ---------------------------------
  -- Numbers definition
  ---------------------------------

  -- All sorts of numbers
  number = c 'number', any {
    float,
    hexadecimal,
    digit^1
  }


  -- anything with special meaning, some are operators
  special = c 'special', word('true', 'false', '_' )

  ---------------------------------
  -- Strings
  ---------------------------------

  -- incl. binary strings? covers multiline strings as well
  str = any {
    span('"', '"', '\\')
    span("'", "'", '\\')
    span('`', '`', '\\')
    span('<<', '>>', '\\')
  }
  string = c 'string', str

  -- does Elixir use this ? Copied from Javascript
  regex = sequence {
    c('regex', sequence {
      '/',
      scan_to(any('/', eol), '\\'),
      B('/'),
    }),
    c('operator', S'gim'^1)^0,
    #sequence {
      blank^0,
      any(S',;.)', P(-1))
    }
  }


  ---------------------------------
  -- Types
  ---------------------------------


  -- not sure about this
  -- type = c 'type', upper^1 * (alpha + digit + '_')^0


  -- the str part is probably not neccessary
  atom = c 'key', any(str, ident) * ':'


  ---------------------------------
  -- Comments
  ---------------------------------

  -- comment from # until eol, can TODO be worked into this to highlight it?
  comment = c 'comment', P'#' * scan_until(eol)



  any {
    comment,
    atom,
    string,
    regex,
    fdecl,
    moduledef,
    keyword,
    functiondef,
    special,
    operator,
    operator2,
    number,
    -- type,
    identifer,
  }
